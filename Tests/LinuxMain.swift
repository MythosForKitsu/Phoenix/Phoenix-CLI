import XCTest

import Phoenix_CLITests

var tests = [XCTestCaseEntry]()
tests += Phoenix_CLITests.allTests()
XCTMain(tests)